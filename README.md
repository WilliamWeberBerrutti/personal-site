# Personal Site

This is the repo for my personal website's code developed while I learn HTML and CSS. It's currently hosted on [Neocities](https://cyantusk.neocities.org/).
Feel free to use it and to contribute by any means!

Also thanks to [Sadness](https://sadgrl.online) for the [88x31 button tool](https://sadgrl.online/projects/88x31-button-maker.html).

During the development, I also created a small framework to aid me on posting. Its code is located on `scripts` subdir.

The procedure is as follows:

1. Use VSCodium to create a copy of TEMPLATE_POST.html on the same dir it's located. It'll name the copy as "TEMPLATE_POST copy.html"
2. Fill the copy's content with the post. There's no need to change dates, titles, etc. Just add the content text and/or images inside the body, where it's written "Custom text."
3. Run `post.py` to prepare for posting. This script will do some steps: replace strings on the post like title, datetime, etc and add the category image (if it's a blog post), add the post on RSS feed, bind the post to its category and update the "Last update time" of index.html
4. Run `git.py` for committing and pushing the changes to this repo. It has interactivity that requires user permission for both. The commit message follows this format: "Add '{postTitle}'"


The usage of each script is as follows:

### post.py

Aids on News and Blog posting.

Usage:

`$ python scripts/post.py <CATEGORY> <POST TITLE> <FEED ITEM DESCRIPTION> [LANGUAGE]`

`CATEGORY:` is one of the categories of my blog. Valid ones are: ['Learnings', 'Phrases', 'Poems', 'Resources', 'Thoughts', 'Tutorials', 'News', 'Aprendizados', 'Frases', 'Pensamentos', 'Poemas', 'Recursos', 'Tutoriais']

`POST TITLE:` Title of the post. E.g. "My Opinion about Free Software"

`FEED ITEM DESCRIPTION:` succint description of the post for the RSS feed. E.g. "A personal viewpoint of the implications of Free Software on the society."

`LANGUAGE:` Language of the post. Valid ones are: ['en_us', 'pt_br'].


### git.py

Aids on committing and pushing changes to the repo.

`$ python scripts/git.py <COMMIT MSG>`

`COMMIT MSG:` The commit message. E.g. "Add 'My Opinion about Free Software'"


## Licensing

Personal Site project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).

The data files (artwork, sound files, etc) are not covered in this license. They are covered in [Lost Garden License](https://lostgarden.com/2007/03/15/lost-garden-license/), which is essentially [CC-BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/).