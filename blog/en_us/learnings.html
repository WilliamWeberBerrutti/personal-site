<!-- Personal Site - My home on the Internet
Copyright (C) 2022-2025 William Weber Berrutti (aka Cyantusk)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/. -->

<!DOCTYPE html>
<html lang="en-us">

<head>
  <title>
    William Weber Berrutti's Blog - Learnings
  </title>

  <meta charset="UTF-8">
  <meta http-equiv="Content-Security-Policy"
    content="default-src 'none'; frame-ancestors 'none'; font-src 'self'; img-src 'self'; object-src 'none'; script-src 'none'; style-src 'self'; base-uri 'self'; form-action 'self'; require-trusted-types-for 'script'">
  <meta name="referrer" content="no-referrer">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="X-Content-Type-Options" content="nosniff">

  <link rel="icon" type="image/x-icon" href="../images/illustrations/favicon.png">
  <link rel="stylesheet" href="../style.css">
</head>

<body>
  <header>
    <nav>
      <a href="../../index.html" class="button" id="Learnings_BackButton">Main Site</a>
      <a href="index.html" class="button" id="Learnings_HomeButton">Home</a>
      <a href="phrases.html" class="button" id="Learnings_PhrasesButton">Phrases</a>
      <a href="thoughts.html" class="button" id="Learnings_ThoughtsButton">Thoughts</a>
      <a href="learnings.html" class="button" id="Learnings_LearningsButton">Learnings</a>
      <a href="tutorials.html" class="button" id="Learnings_TutorialsButton">Tutorials</a>
      <a href="poems.html" class="button" id="Learnings_PoemsButton">Poems</a>
      <a href="resources.html" class="button" id="Learnings_ResourcesButton">Resources</a>
    </nav>
  </header>

  <main>

    <h1>Learnings</h1>

    <img class="picture_illustration_small" src="../images/illustrations/learnings.png" loading="lazy"
      alt="Blog's Learnings Illustration Image">

    <p class="learnings_last_update_date">
      <strong>Last update on:</strong> 27 Jan 2023
    </p>

    <p class="circle_button_note">
      <em><strong>NOTE:</strong> You can right-click the circular button and copy the link to get the corresponding
        item reference.</em>
    </p>

    <div class="container_phrases">

      <div class="container_learnings_element">
        <p id="1"><a href="#1" class="button_other" aria-label="Learning 1">1</a></p>
        <p>
          The view we have of ourselves is usually not the same as the view that others have of us, especially in the
          matter of demand. We tend to perceive ourselves more critically because we know our moral weaknesses better
          than others.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="2"><a href="#2" class="button_other" aria-label="Learning 2">2</a></p>
        <p>
          We need to identify mental saboteurs and eliminate them from our lives. There are several, among them:
          thinking that we are insufficient, charging ourselves too much, thinking excessively about specific aspects of
          life, being conformed by fear, not taking risks to stay in the comfort zone.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="3"><a href="#3" class="button_other" aria-label="Learning 3">3</a></p>
        <p>
          Whenever possible, have time for your friends and family, and don't allow obligations to stop you from
          spending time with them, as obligations will not be helping you in times of distress and loneliness.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="4"><a href="#4" class="button_other" aria-label="Learning 4">4</a></p>
        <p>
          Try to watch over the truth, but always with love, because we don't know who also has the same attitude but
          doesn't have the same level of perception and knowledge as you.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="5"><a href="#5" class="button_other" aria-label="Learning 5">5</a></p>
        <p>
          Do not allow ego influences to harm your friendship relationships, otherwise you will interact with your
          friends out of interest, not brotherhood.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="6"><a href="#6" class="button_other" aria-label="Learning 6">6</a></p>
        <p>
          Be selective, but not too much, so that the imperfections of others don't stop you from getting closer to
          important people in your life and you don't suffer from loneliness when you need the next one the most.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="7"><a href="#7" class="button_other" aria-label="Learning 7">7</a></p>
        <p>
          Be wise in spending time with people. There are some that are not worth our time. Identify friends who really
          are and spend more time with them, including opening up to them in times of emotional need.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="8"><a href="#8" class="button_other" aria-label="Learning 8">8</a></p>
        <p>
          If you need guidance, don't hesitate to ask for help. Don't be so proud that you think you're self-sufficient.
          Nobody is. Nobody is obliged to know everything, to have all the answers. Nobody has them all. We need each
          other. We are social beings and we depend on social interactions to live.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="9"><a href="#9" class="button_other" aria-label="Learning 9">9</a></p>
        <p>
          Do not be afraid to make mistakes. Making mistakes is not necessarily bad. Bad is not learning from mistakes.
          Defeat is part of life. It is with them that we value victories more and learn to have a more fruitful life.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="10"><a href="#10" class="button_other" aria-label="Learning 10">10</a></p>
        <p>
          Don't be a perfectionist. The problem with perfectionism is reaching a state that, by definition, can never be
          reached by imperfect beings like us. Instead, do your tasks as well as possible and notice the quality of the
          resulting work.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="11"><a href="#11" class="button_other" aria-label="Learning 11">11</a></p>
        <p>
          It's perfectly normal for the first few works to be bad. But the more works you do, the more experience you
          get to do them, and soon the time will come when you'll be able to do quality work more easily, because you've
          gained experience and learned more. We must not doubt our abilities. We all have potential.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="12"><a href="#12" class="button_other" aria-label="Learning 12">12</a></p>
        <p>
          Take time for yourself. By spending time alone, you develop your mind and reflect on life. Loneliness is very
          attractive, so be careful not to become antisocial and individualistic.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="13"><a href="#13" class="button_other" aria-label="Learning 13">13</a></p>
        <p>
          Value the privacy of your data and strive to preserve it in the same way that you value your personal safety
          and take steps to protect what is valuable to you. In the wrong hands, your data can be used to control you
          (like putting you in echo chambers, censoring, manipulating, life risks, etc). Avoid companies that exploit
          user data unscrupulously, or use their technologies very cautiously. There are very good alternative
          technologies that help preserve privacy and the human identity.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="14"><a href="#14" class="button_other" aria-label="Learning 14">14</a></p>
        <p>
          Don't compare yourself to others. We are all different, each with different abilities and situations. Compare
          yourself with yourself in the past and see if you are making progress. If not, reflect on your life and the
          moment you are in and try to find out if in fact you are not progressing and what happened for you to be in
          it. Conversations with people who esteem you will help you understand this better.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="15"><a href="#15" class="button_other" aria-label="Learning 15">15</a></p>
        <p>
          Keep your focus on the aspects that you deem most important in your life, so that you don't get anxious about
          your future or depressed about your past. Take care that what you consider important is not centered on
          yourself, but rather on God, who is eternal, so that after your death you leave a good legacy to others.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="16"><a href="#16" class="button_other" aria-label="Learning 16">16</a></p>
        <p>
          We shouldn't focus too much on the things we don't have, but on the things we have, so that gratitude to God
          develops in us. If we don't have something, it's possible that we don't need it at that moment. Maybe not even
          in the future.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="17"><a href="#17" class="button_other" aria-label="Learning 17">17</a></p>
        <p>
          Keep an open mind. They find the truth more easily. But not so open as to forget the principles that define
          the understanding of the human being and of God.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="18"><a href="#18" class="button_other" aria-label="Learning 18">18</a></p>
        <p>
          Think outside the box to make the most of your potential for the glory of God. Don't restrict your creativity
          by the limitations of the environment or the thinking of others. It is necessary to take risks and get out of
          the comfort zone, as it generates stagnation and future regret for not having enjoyed life better.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="19"><a href="#19" class="button_other" aria-label="Learning 19">19</a></p>
        <p>
          Learn to appreciate the simple things in life to live more fully and be more grateful to God, because life is
          mostly made up of ordinary events, not grandiose events. After all, it's the simple things that matter,
          because they make us human.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="20"><a href="#20" class="button_other" aria-label="Learning 20">20</a></p>
        <p>
          We must know how to prioritize our tasks well and manage the time well on them. We must think about the impact
          they will have in the short and long term when deciding the amount of time we will invest in them, as we often
          spend more time on a less important activity instead of a greater one, hampering the progress of the more
          important ones.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="21"><a href="#21" class="button_other" aria-label="Learning 21">21</a></p>
        <p>
          We need to be relaxed to perform our activities well. The more stressed we are, the more likely we are to make
          mistakes and work poorly.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="22"><a href="#22" class="button_other" aria-label="Learning 22">22</a></p>
        <p>
          We should not think that we are out of time to carry out our activities. This is a matter of priority.
          Obviously, we may have several tasks to do, being necessary to postpone the lower priority ones, but that
          doesn't mean we're out of time.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="23"><a href="#23" class="button_other" aria-label="Learning 23">23</a></p>
        <p>
          Often, we need to "embrace boredom" for our brain to "reset" and "reorganize". We have so many activities that
          we often don't stop to think, and these moments are vital for the brain to return to a more "neutral" state.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="24"><a href="#24" class="button_other" aria-label="Learning 24">24</a></p>
        <p>
          We must not think that everything we say can be misinterpreted. Only those who deliberately misinterpret what
          we say have bad intentions, even if you express yourself correctly.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="25"><a href="#25" class="button_other" aria-label="Learning 25">25</a></p>
        <p>
          We need time to relax. We are not machines. We shouldn't spend too many days in "high performance" mode so
          that it doesn't generate emotional exhaustion, such as burnout. Our life is not just about work.
        </p>
      </div>

      <div class="container_learnings_element">
        <p id="26"><a href="#26" class="button_other" aria-label="Learning 26">26</a></p>
        <p>
          We should try to do new things daily to make our days more interesting and less monotonous.
        </p>
      </div>

    </div>

  </main>

  <footer>
    <p>
      Site design graphics originally made by: Dreamforge Intertainment (from <a
        href="https://wikipedia.org/wiki/War_Wind">War Wind</a>), ripped by: <a
        href="https://moonpearl-gm.blogspot.com/2017/09/war-wind-sprites-rip.html">Moonpearl</a> and
      Cyantusk, edited by: <a href="https://cyantusk.neocities.org/">Cyantusk</a>
    </p>
    <a href="https://creativecommons.org/licenses/by/4.0/" rel="license">
      <img class="creative_commons_license" src="../images/licenses/cc_by_40.png" loading="lazy"
        alt="Creative Commons License">
    </a>
    <p>
      The posts of this blog are licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/4.0/">
        Creative Commons Attribution 4.0 International License</a>
    </p>
    <p>
      <a href="https://gitlab.com/WilliamWeberBerrutti/personal-site.git">View site source code</a>
    </p>
  </footer>

</body>

</html>