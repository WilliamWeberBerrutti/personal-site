# Personal Site - My home on the Internet
# Copyright (C) 2022-2025  William Weber Berrutti (aka Cyantusk)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess as sp
import sys
import os

NUM_ARGS = 2


def runGitCommands(commitMsg):
    """
    Runs git commands to commit the new changes.
    
    -----------
    -  Input  -
    ===========
    
    commitMsg: The commit message.
    Type: string
    
    
    ------------
    -  Output  -
    ============
    
    None
    
    """
    
    option = input("Changes are ready to be commited. Proceed? [Y/n] ")
    
    if (option.lower() == "y" or option == ""):
        sp.call(["git", "add", "."])
        sp.call(["git", "commit", "-m", f"{commitMsg}"])
        sp.call(["git", "status"])
        sp.call(["git", "log", "--oneline"])
        
        option = input("Changes are ready to be pushed. Proceed? [Y/n] ")
        
        if (option.lower() == "y" or option == ""):
            sp.call(["git", "push", "origin"])
            sp.call(["git", "status"])
            sp.call(["git", "log", "--oneline"])


def main():
    args = len(sys.argv)
    
    if (args != NUM_ARGS):
        print(f"Not enough args! Usage: python {sys.argv[0]} <COMMIT MSG>")
        return
    
    commitMsg = sys.argv[1]
    
    runGitCommands(commitMsg)
    
    
if __name__ == "__main__":
    main()
