# Personal Site - My home on the Internet
# Copyright (C) 2022-2025  William Weber Berrutti (aka Cyantusk)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from time import gmtime, strftime
from subprocess import call
from codecs import open
from sys import argv
from os import path, chdir
from locale import setlocale, LC_TIME
from string import capwords
from unidecode import unidecode


#### CONSTANTS ####

SITE_ADDRESS = "https://cyantusk.neocities.org"
NUM_ARGS = 5
VALID_CATEGORIES = ["Learnings", "Phrases", "Poems", "Resources", "Thoughts", "Tutorials", "News",
                    "Aprendizados", "Frases", "Pensamentos", "Poemas", "Recursos", "Tutoriais"]
VALID_LANGUAGES = ["en_us", "pt_br"]

scriptUsage = f"""
Usage:

python {path.basename(argv[0])} <CATEGORY> <POST TITLE> <FEED ITEM DESCRIPTION> [LANGUAGE]

where

CATEGORY: is one of the categories of my blog. Valid ones are: {VALID_CATEGORIES}
POST TITLE: Title of the post. E.g. "My Opinion about Free Software"
FEED ITEM DESCRIPTION: succint description of the post for the RSS feed. E.g. "A personal viewpoint of the implications of Free Software on the society."
LANGUAGE: Language of the post. Valid ones are: {VALID_LANGUAGES}.
"""

ILLUSTRATIONS_EN_US = {"Poems":
                           '''    <img class="picture_illustration_small" src="../../images/illustrations/poems.png" loading="lazy"'''
                           + '''\n      alt="Blog's Poems Illustration Image">''',
                       "Thoughts":
                           '''<img class="picture_illustration_small" src="../../images/illustrations/thoughts.png" loading="lazy"'''
                           + '''\n      alt="Blog's Thoughts Illustration Image">''',
                       "Tutorials":
                           '''    <img class="picture_illustration_small" src="../../images/illustrations/tutorials.png" loading="lazy"'''
                           + '''\n      alt="Blog's Tutorials Illustration Image">'''
                      }

ILLUSTRATIONS_PT_BR = {"Poemas":
                           '''<img class="picture_illustration_small" src="../../images/illustrations/poems.png" loading="lazy"'''
                           + '''\n      alt="Imagem Ilustrativa de Poemas do Blog">''',
                       "Pensamentos":
                           '''<img class="picture_illustration_small" src="../../images/illustrations/thoughts.png" loading="lazy"'''
                           + '''\n      alt="Imagem Ilustrativa de Pensamentos do Blog">''',
                       "Tutoriais":
                           '''<img class="picture_illustration_small" src="../../images/illustrations/tutorials.png" loading="lazy"'''
                           + '''\n      alt="Imagem Ilustrativa de Tutoriais do Blog">'''
                      }

ILLUSTRATIONS = {"en_us": ILLUSTRATIONS_EN_US,
                 "pt_br": ILLUSTRATIONS_PT_BR}


#### FUNCTIONS ####

def createFeedEntryXML(category, postTitle, curTime, htmlPageName, feedItemDescription, language):
    """
    Creates an item in the RSS feed file.
    
    -----------
    -  Input  -
    ===========
    
    category: The category the post page belongs.
    Type: string
    
    postTitle: The title of the post.
    Type: string
    
    curTime: The current time for the post.
    Type: time.struct_time
    
    htmlPageName: The page's name
    Type: string
    
    feedItemDescription: description to be used for the new entry item of the RSS feed.
    Type: string
    
    
    ------------
    -  Output  -
    ============
    
    None
    
    """
    
    if (category == "News"):
        baseStr = f"{SITE_ADDRESS}/{category}/"
        feedLink = feedGUID = f"{baseStr}{strftime('%Y-%m-%d', curTime)}.html"
    else:
        baseStr = f"{SITE_ADDRESS}/blog/{language}/{category}/"
        feedLink = feedGUID = f"{baseStr}{htmlPageName}"
    
    feedPubDate = strftime("%a, %d %b %Y 00:00:01 GMT", curTime)

    xmlTextItem = \
        ('\n'
         '  <item>\n'
         '    <title>TITLE</title>\n'
         '    <guid>GUID_HTML_FILE_DIR</guid>\n'
         '    <link>LINK_HTML_FILE_DIR</link>\n'
         '    <pubDate>PUB_DATE_RFC822</pubDate>\n'
         '    <description>\n'
         '      DESCRIPTION\n'
         '    </description>\n'
         '  </item>\n')

    xmlTextItem = xmlTextItem.replace("GUID_HTML_FILE_DIR", feedGUID.lower())
    xmlTextItem = xmlTextItem.replace("LINK_HTML_FILE_DIR", feedLink.lower())
    xmlTextItem = xmlTextItem.replace("PUB_DATE_RFC822", feedPubDate)
    xmlTextItem = xmlTextItem.replace("TITLE", postTitle)
    xmlTextItem = xmlTextItem.replace("DESCRIPTION", feedItemDescription)
    
    feedFileName = "feed.xml"
    
    with open(feedFileName, "r") as f:
        xmlText = f.read()
    
    feedTextToReplace = '</language>\n'

    xmlText = xmlText.replace(feedTextToReplace, feedTextToReplace + xmlTextItem)
    
    with open(feedFileName, "w") as f:
        f.write(xmlText)


def insertOnCategoryPageHTML(category, postTitle, curTime, htmlPageName, htmlTextToReplace, htmlCategoryFileName, language="en_us"):
    """
    Inserts an entry corresponding to the new post on the specified category page.
    
    -----------
    -  Input  -
    ===========
    
    category: The category the post page belongs.
    Type: string
    
    postTitle: The title of the post.
    Type: string
    
    curTime: The current time for the post.
    Type: time.struct_time
    
    htmlPageName: The page's name
    Type: string
    
    htmlTextToReplace: The html text to be replaced on 'htmlPageName'
    Type: string
    
    htmlCategoryFileName: The category's html filename which the new post belongs.
    Type: string
    
    language: The language of the newly created post.
    Type: string
    
    
    ------------
    -  Output  -
    ============
    
    None
    
    """
    
    if (category == "News"):
        htmlCategoryPageTextEntry = \
            ('\n'
             '      <div class="text_news_title">\n'
             '        <p><a href="CATEGORY/HTML_PAGE">\n'
             '          TITLE</a></p>\n'
             '        <p>DATETIME</p>\n'
             '      </div>\n')
    else:
        htmlCategoryPageTextEntry = \
            ('\n'
             '    <div class="text_blog_title">\n'
             '      <p><a href="CATEGORY/HTML_PAGE">\n'
             '        TITLE</a></p>\n'
             '      <p>DATETIME</p>\n'
             '    </div>\n')

    if (language == "en_us"):
        setlocale(LC_TIME, "en_US.UTF-8")
        formattedTime = capwords(strftime("%d %b %Y", curTime))
    elif (language == "pt_br"):
        setlocale(LC_TIME, "pt_BR.UTF-8")
        formattedTime = capwords(strftime("%d %B %Y", curTime)).replace(" ", " de ")
        htmlCategoryPageTextEntry = htmlCategoryPageTextEntry.replace("Date", "Data")
        
    htmlCategoryPageTextEntry = htmlCategoryPageTextEntry.replace("CATEGORY", category.lower())
    htmlCategoryPageTextEntry = htmlCategoryPageTextEntry.replace("HTML_PAGE", htmlPageName)
    htmlCategoryPageTextEntry = htmlCategoryPageTextEntry.replace("TITLE", postTitle)
    htmlCategoryPageTextEntry = htmlCategoryPageTextEntry.replace("DATETIME", formattedTime)

    with open(htmlCategoryFileName, "r") as f:
        htmlCategoryPageText = f.read()
        
    htmlCategoryPageText = htmlCategoryPageText.replace(htmlTextToReplace, htmlTextToReplace + htmlCategoryPageTextEntry)

    with open(htmlCategoryFileName, "w") as f:
        f.write(htmlCategoryPageText)


def updatePostPage(category, postTitle, curTime, htmlPageName, language="en_us"):
    """
    Replaces some strings of the new Post page.
    
    -----------
    -  Input  -
    ===========
    
    category: The category the post page belongs.
    Type: string
    
    postTitle: The title of the post.
    Type: string
    
    htmlPageName: The page's name
    Type: string
    
    language: The language of the newly created post.
    Type: string
    
    
    ------------
    -  Output  -
    ============
    
    Boolean: If the file could be successfully moved to the appropriated directory for further processing.
    
    """
    
    categoryDir = category.lower()
    
    templateFile = "POST_TEMPLATE copy.html"
    
    if (category == "News"):
        templateFile = f"news/{templateFile}"
        
    postPath = f"{categoryDir}/{htmlPageName}"
    
    if (path.exists(templateFile)):
        if (not path.exists(postPath)):
            call(["mv", templateFile, postPath])
        else:
            print(f"[updatePostPage] ERROR: File '{categoryDir}/{htmlPageName}' already exists!")
            return False
    else:
        print(f"[updatePostPage] ERROR: File '{templateFile}' does not exist!")
        return False
    
    with open(postPath, "r") as f:
        htmlPostText = f.read()
    
    htmlPostText = htmlPostText.replace("- CATEGORY", f"- {category}")
    htmlPostText = htmlPostText.replace("CATEGORY", categoryDir)
    htmlPostText = htmlPostText.replace("TITLE", postTitle)
    htmlPostText = htmlPostText.replace("DATENUM", capwords(strftime("%Y-%m-%d", curTime)))
    htmlPostText = htmlPostText.replace("DATETIME", capwords(strftime("%d %b %Y", curTime)))
    
    if (category != "News"):
        htmlPostText = htmlPostText.replace("ILLUSTRATION", ILLUSTRATIONS[language][category])
    
    with open(postPath, "w") as f:
        f.write(htmlPostText)
        
    return True


def updateIndexPage(curTime, language="en_us"):
    """
    Updates Index page Last Update text.
    
    -----------
    -  Input  -
    ===========
    
    curTime: The current time for the post.
    Type: time.struct_time
    
    language: The language of the newly created post.
    Type: string
    
    
    ------------
    -  Output  -
    ============
    
    None
    
    """
    
    if (language == "en_us"):
        htmlIndexLastUpdateDate = '      <strong>Last update on:</strong> '
    elif (language == "pt_br"):
        htmlIndexLastUpdateDate = '      <strong>Última atualização em:</strong> '
        
    with open("index.html", "r") as f:
        htmlIndexPageText = f.read()
        
    htmlIndexPageText = htmlIndexPageText.split("\n")
    
    count = lineIdx = 0
    for count, line in enumerate(htmlIndexPageText):
        if (htmlIndexLastUpdateDate in line):
            lineIdx = count
            break
    
    if (language == "en_us"):
        setlocale(LC_TIME, "en_US.UTF-8")
        htmlIndexPageText[lineIdx] = f"{htmlIndexLastUpdateDate}{capwords(strftime('%d %b %Y', curTime))}"
    elif (language == "pt_br"):
        setlocale(LC_TIME, "pt_BR.UTF-8")
        htmlIndexPageText[lineIdx] = f"{htmlIndexLastUpdateDate}{capwords(strftime('%d %B %Y', curTime)).replace(' ', ' de ')}"
    
    newStr = ""
    for elem in htmlIndexPageText:
        newStr += f"{elem}\n"
    newStr = newStr[:-1]

    with open("index.html", "w") as f:
        f.write(newStr)


def main():
    args = len(argv)
    
    if (args < NUM_ARGS-1 or args > NUM_ARGS):
        print("Not enough args!")
        print(scriptUsage)
        return
    
    category = argv[1]
    
    if (not category in VALID_CATEGORIES):
        print(f"Invalid category! Valid ones are: {VALID_CATEGORIES}")
        return
    
    htmlCategoryFileName = f"{category.lower()}.html"
    postTitle = argv[2]
    feedItemDescription = argv[3]
    
    if (args == NUM_ARGS-1):
        language = "en_us"
    else:
        language = argv[4]
    
    curTime = gmtime()
    
    # TODO: Check if the current year exists already and create a new HTML section automatically if not!
    if (language == "en_us"):
        if (category == "News"):
            htmlTextToReplace = f'    <details>\n' \
                                f'      <summary>{strftime("%Y", curTime)}</summary>\n'
        else:
            htmlTextToReplace = 'Illustration Image">\n'
    elif (language == "pt_br"):
        htmlTextToReplace = 'do Blog">\n'

    htmlPostFileName = unidecode(postTitle.lower().replace(" ", "_"))
    
    if (category == "News"):
        htmlPostFileName = unidecode(strftime("%Y-%m-%d", curTime))
    else:
        htmlPostFileName = unidecode(postTitle.lower().replace(" ", "_"))
        chdir(f"blog/{language}")
        
    htmlPageName = f"{htmlPostFileName}.html"
    
    if (updatePostPage(category, postTitle, curTime, htmlPageName, language)):
        createFeedEntryXML(category, postTitle, curTime, htmlPageName, feedItemDescription, language)
        insertOnCategoryPageHTML(category, postTitle, curTime, htmlPageName, htmlTextToReplace, htmlCategoryFileName, language)
        updateIndexPage(curTime, language)
    
    
if __name__ == "__main__":
    main()
